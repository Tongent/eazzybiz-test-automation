package equityjavaproject;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Payroll {

	public static WebDriver driver;
	public static WebDriverWait wait;
	public static WebElement element;
	public static void main(String[] args) throws InterruptedException, AWTException {
		System.setProperty("webdriver.chrome.driver", "P:\\EazzyBizPortalWorkforce\\equityjavaproject\\lib\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://ebankappuat.ebsafrica.com:7006/iportalweb/iportal/jsps/orbilogin.jsp");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//input[@placeholder='User Name']")).sendKeys("MANIND");
		driver.findElement(By.xpath("//input[@placeholder='Password']")).sendKeys("cbx123");
		Thread.sleep(10000);//Diksha@2020
		driver.findElement(By.xpath("//input[@placeholder='Enter the Captcha text here']")).sendKeys("abcd");
		driver.findElement(By.xpath("//a[contains(text(),'Sign In ')]")).click();
		//driver.findElement(By.xpath("//div[@class='forgot-password-btn preventDrag']")).sendKeys("");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@id='OTP']")).sendKeys("1234");
		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='OTP']")));
		Thread.sleep(7000);
		driver.findElement(By.xpath("//input[@value='Submit']")).click();
		//Enter into Account Services
		WebElement gotoPayment = driver.findElement(By.xpath("//div[@class='widget_icon PYMNTSMaster']"));
		Thread.sleep(8000);
		gotoPayment.click();	
		//Enter into Account Summary
		Thread.sleep(8000);
		driver.findElement(By.xpath("//button[text()='Initiate']")).click();
		Thread.sleep(3000);
		WebElement payroll = driver.findElement(By.xpath("(//span[text()='Payroll'])[1]"));
		//Thread.sleep(2000);
		Thread.sleep(5000);
		Actions action = new Actions(driver);
		action.moveToElement(payroll).perform();
		System.out.println("Enter into third party");
		WebElement gotoThirdPart = driver.findElement(By.xpath("(//span[text()='Payroll'])[2]"));
		Thread.sleep(2000);
		action.moveToElement(gotoThirdPart).click().perform();
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//img[@class='x-form-trigger x-form-arrow-trigger'])[4]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//div[text()='EQUITY']")).click();
		Thread.sleep(3000);
		//upload file
		WebElement file = driver.findElement(By.xpath("//div[@style='position: absolute; overflow: hidden; top: 0px; left: 0px; width: 36px; height: 15px; opacity: 0;']"));
		Thread.sleep(4000);
		file.click();
		Thread.sleep(8000);
		System.out.println("Enter into bulk file");
		Thread.sleep(5000);
		Toolkit t = Toolkit.getDefaultToolkit();
		StringSelection s= new StringSelection("C:\\Users\\Mahinder.Pal\\Downloads\\Within Bank Bulk - WeldonT.xlsx");
		t.getSystemClipboard().setContents(s, s);
		
		Robot robot = new Robot();
		 Thread.sleep(1000);
		      
//		  // Press Enter
//		 robot.keyPress(KeyEvent.VK_ENTER);
//		 
//		// Release Enter
//		 robot.keyRelease(KeyEvent.VK_ENTER);
		 
		  // Press CTRL+V
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		// Release CTRL+V
		 robot.keyRelease(KeyEvent.VK_CONTROL);
		 robot.keyRelease(KeyEvent.VK_V);
		 Thread.sleep(1000);
		        
		  //       Press Enter 
		 robot.keyPress(KeyEvent.VK_ENTER);
		 // robot.keyRelease(KeyEvent.VK_ENTER);
		 Thread.sleep(5000);
		 WebElement transRefrence = driver.findElement(By.xpath("//textarea[@name='PAY_NARRATION']"));
		 transRefrence.sendKeys("Bank Loan");
		 Thread.sleep(3000);
		 driver.findElement(By.xpath("//button[text()='Submit']")).click();
		 Thread.sleep(4000);
		 WebElement conf = driver.findElement(By.xpath("//button[text()='Confirm']"));
		 conf.click();
		 Thread.sleep(4000);
		 driver.findElement(By.xpath("//button[text()='Close']")).click();
	}
}
