package equityjavaproject;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class EquityLoginPortal {
	public static WebDriver driver;
	public static WebDriverWait wait;
	public static WebElement element;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "P:\\EazzyBizPortalWorkforce\\equityjavaproject\\lib\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://ebankappuat.ebsafrica.com:7006/iportalweb/iportal/jsps/orbilogin.jsp");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//input[@placeholder='User Name']")).sendKeys("MANIND");
		driver.findElement(By.xpath("//input[@placeholder='Password']")).sendKeys("cbx123");
		Thread.sleep(10000);//Diksha@2020
		driver.findElement(By.xpath("//input[@placeholder='Enter the Captcha text here']")).sendKeys("abcd");
		driver.findElement(By.xpath("//a[contains(text(),'Sign In ')]")).click();
		//driver.findElement(By.xpath("//div[@class='forgot-password-btn preventDrag']")).sendKeys("");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@id='OTP']")).sendKeys("1234");
		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='OTP']")));
		Thread.sleep(7000);
		driver.findElement(By.xpath("//input[@value='Submit']")).click();
		//Enter into Account Services
		element = driver.findElement(By.xpath("//div[@class='widget_icon ACCSERVMaster']"));
		Thread.sleep(8000);
		element.click();	
		//Enter into Account Summary
		Thread.sleep(8000);
		driver.findElement(By.xpath("(//span[text()='Account Summary'])[1]")).click();
		
		
		//WebDriverWait wait = new WebDriverWait (driver, 20);
		//wait.until(ExpectedConditions.visibilityOf(element));
		Thread.sleep(33000);
		//element = driver.findElement(By.xpath("//div[contains(text(),'0150269445839')]"));
		element = driver.findElement(By.xpath("//div[contains(text(),'0570194093522')]"));
		Actions action = new Actions(driver);
		action.doubleClick(element).perform();
		Thread.sleep(5000);
		
		WebElement enableTrancRange = driver.findElement(By.xpath("(//input[@class=' x-form-radio x-form-field' and @type='radio'])[2]"));
		enableTrancRange.click();
		Thread.sleep(2000);
		WebElement startDate = driver.findElement(By.xpath("(//input[@class='x-form-text x-form-field x-form-dateField x-trigger-noedit'])[1]"));
		startDate.click();
		Thread.sleep(2000);
		WebElement previousMonth = driver.findElement(By.xpath("//a[contains(@title,'Previous Month')]"));
		previousMonth.click();
		Thread.sleep(1000);
		previousMonth.click();
		Thread.sleep(1000);
		previousMonth.click();
		Thread.sleep(1000);
		WebElement previousMonthDate = driver.findElement(By.xpath("(//td[@class='x-date-active'])[1]"));
		previousMonthDate.click();
		Thread.sleep(1000);
		WebElement toDate = driver.findElement(By.xpath("(//input[@class='x-form-text x-form-field x-form-dateField x-trigger-noedit'])[1]"));
		toDate.click();
		Thread.sleep(1000);
		WebElement currentMonthDate = driver.findElement(By.xpath("//td[@class='x-date-active']/a/em/span[text()='1']"));
		currentMonthDate.click();
		Thread.sleep(1000);
		WebElement go = driver.findElement(By.xpath("//button[text()='Go']"));
		go.click();
		Thread.sleep(20000);
		List<WebElement> transTable = driver.findElements(By.xpath("//div[@class='x-grid3-viewport x-masked-relative x-masked']/div[@class='x-grid3-scroller']/div[@class='x-grid3-body']/div"));
		int trans = transTable.size();
		if(trans > 1)
			System.out.println("transaction are populated"+ trans);
		else
			Assert.assertTrue(false, "Transaction table is empty");
		
		//driver.findElement(By.xpath("//div[@class='x-grid3-row x-grid3-row-selected']/table/tbody/tr/td[1]/div[contains(text(),'0150269445839')]")).click();
	}

}
