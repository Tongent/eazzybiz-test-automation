package equityjavaproject;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class BaseTest {

	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest test;
	public static ExtentTest logger;

	@BeforeSuite
	public void launch() throws MalformedURLException, InterruptedException {

		File testDirectory = new File(System.getProperty("user.dir") + "/reports/WebAutomation.html");
		boolean result = false;
		if (!testDirectory.exists()) {
			System.out.println("creating directory: " + testDirectory.getName());

			try {
				testDirectory.createNewFile();
				result = true;
			} catch (SecurityException | IOException se) {
				System.out.println(se);
			}
			if (result) {
				System.out.println("DIR created");
			}
		}
		htmlReporter = new ExtentHtmlReporter("./reports/WebAutomation.html");

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		extent.setSystemInfo("OS", "Windows 10");
		extent.setSystemInfo("TesterName", "Maninder");
		extent.setSystemInfo("Environment", "UAT");

		htmlReporter.config().setChartVisibilityOnOpen(true);
		htmlReporter.config().setDocumentTitle("AutomationTesting in Demo Report");
		htmlReporter.config().setReportName("Eazzy Bizz Web Portal Report");
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.DARK);
	}

	@AfterMethod
	public void getResult(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " Test case FAILED due to below issues:",
					ExtentColor.RED));
			test.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + " Test Case PASSED", ExtentColor.GREEN));
		} else {
			test.log(Status.SKIP,
					MarkupHelper.createLabel(result.getName() + " Test Case SKIPPED", ExtentColor.ORANGE));
			test.skip(result.getThrowable());
		}
	}

	public static String screenshotName;

	public static void getscreenshot(WebDriver driver, String scrnname) {

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Date d = new Date();
		screenshotName = scrnname + d.toString().replace(":", "_").replace(" ", "_") + ".jpg";
		try {
			FileHandler.copy(scrFile, new File(System.getProperty("user.dir") + "\\reports\\" + screenshotName));
		} catch (Exception e) {
			System.out.println("Exception while taking screenshot" + e.getMessage());
		}
	}

	@AfterSuite
	public void tearDown() {
		extent.flush();
	}
}