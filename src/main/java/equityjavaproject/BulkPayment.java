package equityjavaproject;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

public class BulkPayment extends BaseTest {

	public static WebDriver driver;
	public static WebDriverWait wait;
	public static WebElement element;
	
	@Test
	public static void bulkpaymentvalidation() throws InterruptedException, AWTException {
		test = extent.createTest("Equity Bulk Payment");
		System.setProperty("webdriver.chrome.driver", "P:\\EazzyBizPortalWorkforce\\equityjavaproject\\lib\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://ebankappuat.ebsafrica.com:7006/iportalweb/iportal/jsps/orbilogin.jsp");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//input[@placeholder='User Name']")).sendKeys("MANIND");
		driver.findElement(By.xpath("//input[@placeholder='Password']")).sendKeys("cbx123");
		Thread.sleep(10000);//Diksha@2020
		driver.findElement(By.xpath("//input[@placeholder='Enter the Captcha text here']")).sendKeys("abcd");
		driver.findElement(By.xpath("//a[contains(text(),'Sign In ')]")).click();
		logger.log(Status.INFO, "Click on Sign In");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@id='OTP']")).sendKeys("1234");
		logger.log(Status.INFO, "Enter four digit OTP pin");
		Thread.sleep(7000);
		driver.findElement(By.xpath("//input[@value='Submit']")).click();
		logger.log(Status.INFO, "Enter into account Services");
		//Enter into Account Services
		WebElement gotoPayment = driver.findElement(By.xpath("//div[@class='widget_icon PYMNTSMaster']"));
		Thread.sleep(8000);
		gotoPayment.click();	
		//Enter into Account Summary
		Thread.sleep(8000);
		driver.findElement(By.xpath("//button[text()='Initiate']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//span[text()='Bulk Payments']")).click();
		Thread.sleep(6000);
		logger.log(Status.INFO, "Enter into Bulk payment");
		System.out.println("Enter into bulk payment");
		WebElement selectTrans =  driver.findElement(By.xpath("//input[@id='$$_ext-comp-1384']"));
		Thread.sleep(7000);
		selectTrans.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[text()='Normal File Upload']")).click();
		Thread.sleep(2000);
		logger.log(Status.INFO, "Go to File Upload");
		driver.findElement(By.xpath("//input[@id='$$_ext-comp-1386']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[text()='ALEXOTHERPAY']")).click();
		WebElement file = driver.findElement(By.xpath("//div[@style='position: absolute; overflow: hidden; top: 0px; left: 0px; width: 36px; height: 15px; opacity: 0;']"));
		Thread.sleep(4000);
		file.click();
		//driver.findElement(By.xpath("(//div[text()='Normal File Upload'])[2]")).click();
		Thread.sleep(8000);
		System.out.println("Enter into bulk file");
		Thread.sleep(5000);
		Toolkit t = Toolkit.getDefaultToolkit();
		StringSelection s= new StringSelection("C:\\Users\\Mahinder.Pal\\Downloads\\Within Bank Bulk - WeldonT.xlsx");
		t.getSystemClipboard().setContents(s, s);
		logger.log(Status.INFO, "Select the Excel file");
		Robot robot = new Robot();
		 Thread.sleep(1000);
		 
		  // Press CTRL+V
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		// Release CTRL+V
		 robot.keyRelease(KeyEvent.VK_CONTROL);
		 robot.keyRelease(KeyEvent.VK_V);
		 Thread.sleep(1000);
		        
		  //       Press Enter 
		 robot.keyPress(KeyEvent.VK_ENTER);
		 // robot.keyRelease(KeyEvent.VK_ENTER);
		 Thread.sleep(4000);
		 driver.findElement(By.xpath("//img[@class='x-form-trigger x-form-date-trigger']")).click();
		 Thread.sleep(4000);
		 WebElement currDate = driver.findElement(By.xpath("//td[@class='x-date-active']/a/em/span[text()='28']"));
		 currDate.click();
		 logger.log(Status.INFO, "Select current date");
		 WebElement transRefrence = driver.findElement(By.xpath("//textarea[@id='ext-comp-1405']"));
		 Thread.sleep(3000);
		 transRefrence.sendKeys("Bank Loan");
		 logger.log(Status.INFO, "Enter Bank Loan");
		 Thread.sleep(3000);
		 driver.findElement(By.xpath("//button[text()='Submit']")).click();
		 WebElement conf = driver.findElement(By.xpath("//button[text()='Confirm']"));
		 Thread.sleep(4000);
		 conf.click();
		 logger.log(Status.INFO, "Click on confirm Button");
		 Thread.sleep(4000);
		 driver.findElement(By.xpath("//button[text()='Close']")).click();
		 logger.log(Status.INFO, "Click on close");
		 
	}
	}

