package equityjavaproject;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

public class Payment extends BaseTest {
	
	public static WebDriver driver;
	public static WebDriverWait wait;
	public static WebElement element;
	
	@Test
	public static void paymentvalidation() throws InterruptedException {
		test = extent.createTest("Equity Payment module");
		System.setProperty("webdriver.chrome.driver", "P:\\EazzyBizPortalWorkforce\\equityjavaproject\\lib\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://ebankappuat.ebsafrica.com:7006/iportalweb/iportal/jsps/orbilogin.jsp");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//input[@placeholder='User Name']")).sendKeys("MANIND");
		driver.findElement(By.xpath("//input[@placeholder='Password']")).sendKeys("cbx123");
		Thread.sleep(10000);//Diksha@2020
		driver.findElement(By.xpath("//input[@placeholder='Enter the Captcha text here']")).sendKeys("abcd");
		driver.findElement(By.xpath("//a[contains(text(),'Sign In ')]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@id='OTP']")).sendKeys("1234");
		logger.log(Status.INFO, "Enter four digit OTP");
		Thread.sleep(7000);
		driver.findElement(By.xpath("//input[@value='Submit']")).click();
		logger.log(Status.INFO, "Enter into Account summary");
		//Enter into Account Services
		WebElement gotoPayment = driver.findElement(By.xpath("//div[@class='widget_icon PYMNTSMaster']"));
		Thread.sleep(8000);
		logger.log(Status.INFO, "Go to Payment");
		gotoPayment.click();	
		//Enter into Account Summary
		Thread.sleep(8000);
		driver.findElement(By.xpath("//button[text()='Initiate']")).click();
		Thread.sleep(3000);
		WebElement payTo = driver.findElement(By.xpath("(//span[text()='Payments'])[3]"));
		//Thread.sleep(2000);
		Thread.sleep(5000);
		Actions action = new Actions(driver);
		action.moveToElement(payTo).perform();
		logger.log(Status.INFO, "Enter into third party");
		WebElement gotoThirdPart = driver.findElement(By.xpath("//span[text()='Intra Bank Fund Transfer (Third Party)']"));
		Thread.sleep(2000);
		action.moveToElement(gotoThirdPart).click().perform();
		WebElement search = driver.findElement(By.xpath("(//img[@class='x-form-trigger undefined'])[1]"));
		Thread.sleep(8000);
		search.click();
		Thread.sleep(59000);
		WebElement debitAcc = driver.findElement(By.xpath("//div[text()='0180293047332']"));
		JavascriptExecutor jse2 = (JavascriptExecutor)driver;
		jse2.executeScript("arguments[0].scrollIntoView()", debitAcc); 
		action.moveToElement(debitAcc).click().build().perform();
		logger.log(Status.INFO, "Enter into Debit Account");
		Thread.sleep(13000);
		action.doubleClick(debitAcc).perform();
		WebElement enableRegist = driver.findElement(By.xpath("//input[@id='ext-comp-1503']"));
		enableRegist.click();
		logger.log(Status.INFO, "Enter into Debit Account");
		Thread.sleep(10000);
		driver.findElement(By.xpath("//img[@id='ext-gen645']")).click();
		WebElement beneficaryAcc = driver.findElement(By.xpath("(//div[text()='0170161209441'])[2]"));
		Thread.sleep(12000);
		action.doubleClick(beneficaryAcc).perform();
	}
}
